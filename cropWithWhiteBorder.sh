#!/bin/bash

# Zuschneiden von allen Bildern eines Ordners mittels convert
# 1: Quellpfad (ohne letzten /)
# 2: Zielpfad (ohne letzten /)
# 3: B: Breite
# 4: H: Hoehe
# 5: b: Abstand linker Rand
# 6: h: Abstand oberer Rand
# 7: Dateiendung
# 8: nPixel white border


quellpfad=${1}'/*.'${7}

for pic in $quellpfad
    do
	    picname=$(basename "$pic")
	    echo "Bearbeite Bild:    $picname"
            zielpfad=${2}/$picname
	    convert "$pic" -crop $3x$4+$5+$6 -bordercolor white -border $8 $zielpfad
	done
