#!/bin/bash

# 1: Quellpfad (ohne letzten /)
# 2: Zielpfad (ohne letzten /)
# 3: Dateiendung


quellpfad=${1}'/*.'${3}

for pic in $quellpfad
    do
        picname=$(basename "$pic")
        echo "Trimme Bild:    $picname"
        zielpfad=${2}/$picname
        convert "$pic" -trim $zielpfad
    done
