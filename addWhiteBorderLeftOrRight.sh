#!/bin/bash

# Zuschneiden von allen Bildern eines Ordners mittels convert
# 1: Quellpfad
# 2: Zielpfad
# 3: Dateiendung
# 4: "east" oder "west": Angabe, ob Pixel oben oder unten hinzugefuegt werden sollen
# 5: dx: Anzahl Pixel, die unten hinzugefuegt werden sollen


quellpfad=${1}'*.'${3}

for pic in $quellpfad
    do
	    picname=$(basename "$pic")
	    echo "Bearbeite Bild:    $picname"
            zielpfad=${2}$picname
	    convert "$pic" -gravity ${4} -splice ${5}x0 $zielpfad
	done
