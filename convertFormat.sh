#!/bin/bash

# Format aller Bilder eines Ordners mittels convert √§ndern

# 1: Quellpfad (mit letztem /)
# 2: Zielpfad (mit letztem /)
# 3: Dateiendung Quellpfad
# 4: Dateiendung Zielpfad


quellpfad=${1}'*.'${3}



for pic in $quellpfad
    do
         picname=$(basename "$pic" ".${3}")
         newname=$picname.${4}

         echo "Bearbeite Bild:    $pic  --->  $newname"

         convert "$pic" "${2}$newname"
    done
